#ifndef DATABASEHELPER_H
#define DATABASEHELPER_H

#include <QObject>

 enum Module {
     Notes,
     Triade,
     Interval
 };

class DataBaseHelper : public QObject
{
    Q_OBJECT
public:
    explicit DataBaseHelper(QObject *parent = 0);

protected:
static QString basePath();
signals:

public slots:
    static QString urlForNote(const QString& note);
    static QString urlForNote(const QString& note, const QString& triad);
    static QString urlForNoteAndInterval(const QString& note, const QString& interval);
};

#endif // DATABASEHELPER_H
