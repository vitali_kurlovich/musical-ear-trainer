#include "notedetectorform.h"
#include "ui_notedetectorform.h"
#include "databasehelper.h"
#include "utils.h"

#include <QSoundEffect>
#include <QDebug>

NoteDetectorForm::NoteDetectorForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NoteDetectorForm)
{
    ui->setupUi(this);
    doShot();
}

NoteDetectorForm::~NoteDetectorForm()
{
    delete ui;
}

// кнопка 'Продолжить'
void NoteDetectorForm::clickNext() {
    doShot();
}

// генерируем случайный вариант
void NoteDetectorForm::doShot() {
    currentNote = rand()%7 + 1; //  случайная нота

    // настройка  интерфейса
    ui->nextButton->setEnabled(false);
    ui->result_label->setText("");
    ui->answerLabel->setText("");

    qDebug() << "Current note:" << currentNote;
    playSoundForNote(Utils::codeForNote(currentNote)); // проигрываем сгенерированный вариант
    enableNoteButtons(true); // делаем активными кнопки выбора варианта
}

bool NoteDetectorForm::isCorrectNote(int note) {
    return (note == currentNote);
}

void NoteDetectorForm::checkNote(int note) {

    qDebug() << "Check note:" << note;
    // обработка результата
    if (isCorrectNote(note)) {
        ui->result_label->setText("Правильно");
        ui->result_label->setStyleSheet("color:green");
        ui->answerLabel->setText(tr("Правильный ответ: Нота %1").arg(Utils::nameForNote(currentNote)));
    } else {
        ui->result_label->setText("Не правильно");
        ui->result_label->setStyleSheet("color:red");
        ui->answerLabel->setText(tr("Правильный ответ: Нота %1").arg(Utils::nameForNote(currentNote)));
    }

    ui->nextButton->setEnabled(true); // делаем активной кнопку 'Продолжить'
    enableNoteButtons(false); // делаем не активными кнопки выбора варианта
}


// установка свойство enable для кнопок выбора ответа
void NoteDetectorForm::enableNoteButtons(bool enable) {
    ui->pushButtonA->setEnabled(enable);
    ui->pushButtonB->setEnabled(enable);
    ui->pushButtonC->setEnabled(enable);
    ui->pushButtonD->setEnabled(enable);
    ui->pushButtonE->setEnabled(enable);
    ui->pushButtonF->setEnabled(enable);
    ui->pushButtonG->setEnabled(enable);
}

void NoteDetectorForm::clickBack() {
    // назад
    emit onBack();
}

// нажали на кнопку 'До'
void NoteDetectorForm::clickA() {

    playSoundForNote(Utils::codeForNote(1)); // проиграли звук связанный с нотой   'До'
    checkNote(1); // проверили правильность выбора для ноты  'До'
}

// нажали на кнопку 'Ре'
void NoteDetectorForm::clickB() {
    playSoundForNote(Utils::codeForNote(2)); //
    checkNote(2);
}

// 'Ми'
void NoteDetectorForm::clickC() {
    playSoundForNote(Utils::codeForNote(3));
    checkNote(3);
}

// 'Фа'
void NoteDetectorForm::clickD() {
    playSoundForNote(Utils::codeForNote(4));
    checkNote(4);
}

// 'Соль'
void NoteDetectorForm::clickE() {
    playSoundForNote(Utils::codeForNote(5));
    checkNote(5);
}

// 'Ля'
void NoteDetectorForm::clickF() {
    playSoundForNote(Utils::codeForNote(6));
    checkNote(6);
}

// 'Си'
void NoteDetectorForm::clickG() {
    playSoundForNote(Utils::codeForNote(7));
    checkNote(7);
}

// кнопка 'Повторить'
void NoteDetectorForm::clickReplay() {
    playSoundForNote(Utils::codeForNote(currentNote)); // проиграли текущую ноту
}

// проиграть ноту
void NoteDetectorForm::playSoundForNote(const QString& note) const {

    QString url = DataBaseHelper::urlForNote(note); // получаем из БД путь к звуковому файлу
    qDebug() << url;

    if (!url.isEmpty()) { // если путь не пустой проигрываем звук
        QSoundEffect* sound = new QSoundEffect();
        sound->setSource(QUrl::fromLocalFile(url));
        sound->play();
    }
}
