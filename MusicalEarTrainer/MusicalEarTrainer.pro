#-------------------------------------------------
#
# Project created by QtCreator 2014-05-26T12:52:37
#
#-------------------------------------------------

QT       += core  multimedia sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MusicalEarTrainer
TEMPLATE = app


SOURCES += main.cpp\
    triaddetectorform.cpp \
    intervaldetectorform.cpp \
    notedetectorform.cpp \
    choicetrainingform.cpp \
    mainwindow.cpp \
    databasehelper.cpp \
    utils.cpp

HEADERS  += \
    triaddetectorform.h \
    intervaldetectorform.h \
    notedetectorform.h \
    choicetrainingform.h \
    mainwindow.h \
    databasehelper.h \
    utils.h

FORMS    += \
    triaddetectorform.ui \
    intervaldetectorform.ui \
    notedetectorform.ui \
    choicetrainingform.ui \
    mainwindow.ui

RESOURCES += \
    Resources/Resources.qrc

OTHER_FILES += \
    data/database.dat


database.files = Resources/database.dat
database.target = .

INSTALLS += database

