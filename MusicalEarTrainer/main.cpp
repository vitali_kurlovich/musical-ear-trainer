#include "mainwindow.h"
#include <QApplication>
#include <QSqlDatabase>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // путь к базе данных
    QString dataBasePath = "./data/database.dat";

    qDebug() << "Database paht: "<<dataBasePath;
    QSqlDatabase dbase = QSqlDatabase::addDatabase("QSQLITE");
    dbase.setDatabaseName(dataBasePath);

    if (!dbase.open()) {
        // не смогли открыть базу данных
        qDebug() << "Что-то не так с соединением!";
        return -1;
    }

    // имена всех таблиц
    QStringList tables = dbase.tables(QSql::AllTables);

    foreach (QString name, tables) {
        qDebug() << name;
    }

    MainWindow w;
    w.show();

    return a.exec();
}
