#ifndef INTERVALDETECTORFORM_H
#define INTERVALDETECTORFORM_H

#include <QWidget>

namespace Ui {
class IntervalDetectorForm;
}

class IntervalDetectorForm : public QWidget
{
    Q_OBJECT

public:
    explicit IntervalDetectorForm(QWidget *parent = 0);
    ~IntervalDetectorForm();

signals:
    void onBack();

protected slots:
    void clickBack();

protected slots:
    void clickNext();
    void clickReplay();

protected slots:
    void click1();
    void click2();
    void click3();
    void click4();
    void click5();
    void click6();
    void click7();
    void click8();
    void click9();
    void click10();
    void click11();
    void click12();

    //hijklm

protected:
    int currentNote;
    int currentInterval;

    bool isCorrectInterval(const int interval) const;
    void checkInterval(const int interval) const;

    void doShot();

    void enableButtons(bool enable) const;
    void playSoundForNote(const QString& note, const QString& interval) const;
private:
    Ui::IntervalDetectorForm *ui;
};

#endif // INTERVALDETECTORFORM_H
