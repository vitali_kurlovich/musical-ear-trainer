#ifndef TRIADDETECTORFORM_H
#define TRIADDETECTORFORM_H

#include <QWidget>

namespace Ui {
class TriadDetectorForm;
}

class TriadDetectorForm : public QWidget
{
    Q_OBJECT

public:
    explicit TriadDetectorForm(QWidget *parent = 0);
    ~TriadDetectorForm();

signals:
    void onBack();

protected slots:
    void clickBack();

protected slots:
    void clickNext();
    void clickReplay();

protected slots:
    void clickMinor();
    void clickMajor();
protected:
    int currentNote;
    int currentTriade;

   void enableTriadButtons(bool enable) const;

   void doShot();

   bool isCorrectTriad(const int triad) const;
   void checkTriad(const int triad) const;

   void playSoundForNote(const QString& note, const QString triad) const;

private:
    Ui::TriadDetectorForm *ui;
};

#endif // TRIADDETECTORFORM_H
