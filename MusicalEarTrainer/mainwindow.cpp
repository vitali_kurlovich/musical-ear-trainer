#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "choicetrainingform.h"

#include "intervaldetectorform.h"
#include "notedetectorform.h"
#include "triaddetectorform.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ///ui->setupUi(this);
    showChoiceTrainingForm();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showChoiceTrainingForm() {
    // форма главного экрана
    ChoiceTrainingForm* form = new ChoiceTrainingForm();
    // настраиваем сигналы завязаные на кнопки выбора урока
    connect(form, SIGNAL(onIntervalDetector()), this, SLOT(showIntervalDetector()));
    connect(form, SIGNAL(onTriadeDetector()), this, SLOT(showTriadeDetector()));
    connect(form, SIGNAL(onNoteDetector()), this, SLOT(showNoteDetector()));

    setWindowTitle("Музыкальный тренажер");

    this->setCentralWidget(form);
}


void MainWindow::showIntervalDetector() {
    // показываем интерфейс для "Определение интервалов"
    IntervalDetectorForm* form = new IntervalDetectorForm();
    connect(form, SIGNAL(onBack()), this, SLOT(showChoiceTrainingForm())); // сигнал для возврата в главное меню
    this->setCentralWidget(form);

    setWindowTitle("Определение интервалов");
}

void MainWindow::showTriadeDetector() {
    // показываем интерфейс для "Определение трезвучий"
    TriadDetectorForm* form = new TriadDetectorForm();
    connect(form, SIGNAL(onBack()), this, SLOT(showChoiceTrainingForm())); // сигнал для возврата в главное меню
    this->setCentralWidget(form);

    setWindowTitle("Определение трезвучий");
}

void MainWindow::showNoteDetector() {
    // показываем интерфейс для "Определение нот"
    NoteDetectorForm* form = new NoteDetectorForm();
    connect(form, SIGNAL(onBack()), this, SLOT(showChoiceTrainingForm())); // сигнал для возврата в главное меню
    this->setCentralWidget(form);

    setWindowTitle("Определение нот");
}
