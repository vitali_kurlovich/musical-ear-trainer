#ifndef NOTEDETECTORFORM_H
#define NOTEDETECTORFORM_H

#include <QWidget>


namespace Ui {
class NoteDetectorForm;
}

class NoteDetectorForm : public QWidget
{
    Q_OBJECT

public:
    explicit NoteDetectorForm(QWidget *parent = 0);
    ~NoteDetectorForm();

protected:

signals:
    void onBack();

protected slots:
    void clickBack();

protected slots:
    void clickA();
    void clickB();
    void clickC();
    void clickD();
    void clickE();
    void clickF();
    void clickG();

protected slots:
    void clickNext();
    void clickReplay();

protected:
    int currentNote;
    bool isCorrectNote(int note);
    void checkNote(int note);

    void doShot();

    QString keyForNote(int note);
    QString nameForNote(int note);

    void enableNoteButtons(bool enable);
    void playSoundForNote(const QString& note) const;

private:
    Ui::NoteDetectorForm *ui;
};

#endif // NOTEDETECTORFORM_H
