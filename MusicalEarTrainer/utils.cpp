#include "utils.h"

Utils::Utils(QObject *parent) :
    QObject(parent)
{
}

// код ноты по ее индексу
QString Utils::codeForNote(const int note) {
    QString key;
    switch (note) {
    case 1:
        key = "a";
        break;
    case 2:
        key = "b";
        break;
    case 3:
        key = "c";
        break;
    case 4:
        key = "d";
        break;
    case 5:
        key = "e";
        break;
    case 6:
        key = "f";
        break;
    case 7:
        key = "g";
        break;
    default:
        key = "";
        break;
    }
    return key;
}

// имя ноты по индексу
QString Utils::nameForNote(const int note) {
    QString name;
    switch (note) {
    case 1:
        name = "До";
        break;
    case 2:
        name = "Ре";
        break;
    case 3:
        name = "Ми";
        break;
    case 4:
        name = "Фа";
        break;
    case 5:
        name = "Соль";
        break;
    case 6:
        name = "Ля";
        break;
    case 7:
        name = "Си";
        break;
    default:
        name = "";
        break;
    }
    return name;
}

// код для триады
QString Utils::codeForTriad(const int triad) {
    if (triad == 1) {
        return "major";
    }

    if (triad == 2) {
        return "minor";
    }

    return "";
}

// имя для триады по индексу
QString Utils::nameForTriad(const int triad) {
    if (triad == 1) {
        return "Мажроное трезвучие";
    }

    if (triad == 2) {
        return "Минорное трезвучие";
    }

    return "";
}

// код для интервала
QString Utils::codeForInterval(const int interval) {
    QString key;
    switch (interval) {
    case 1:
        key = "perfect_unison";
        break;
    case 2:
        key = "minor_second";
        break;
    case 3:
        key = "major_second";
        break;
    case 4:
        key = "minor_third";
        break;
    case 5:
        key = "major_third";
        break;
    case 6:
        key = "perfect_fourth";
        break;
    case 7:
        key = "perfect_fifth";
        break;
    case 8:
        key = "minor_sixth";
        break;
    case 9:
        key = "major_sixth";
        break;
    case 10:
        key = "minor_seventh";
        break;
    case 11:
        key = "major_seventh";
        break;
    case 12:
        key = "perfect_octave";
        break;
    default:
        key = "";
        break;
    }
    return key;
}

// имя для интервала
QString Utils::nameForInterval(const int interval) {
    QString name;
    switch (interval) {
    case 1:
        name = "Чистая прима";
        break;
    case 2:
        name = "Малая секунда";
        break;
    case 3:
        name = "Большая секунда";
        break;
    case 4:
        name = "Малая терция";
        break;
    case 5:
        name = "Большая терция";
        break;
    case 6:
        name = "Чистая кварта";
        break;
    case 7:
        name = "Чистая квинта";
        break;
    case 8:
        name = "Малая секста";
        break;
    case 9:
        name = "Большая секста";
        break;
    case 10:
        name = "Малая септима";
        break;
    case 11:
        name = "Большая септима";
        break;
    case 12:
        name = "Октава";
        break;
    default:
        name = "";
        break;
    }
    return name;
}
