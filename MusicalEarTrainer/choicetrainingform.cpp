#include "choicetrainingform.h"
#include "ui_choicetrainingform.h"

ChoiceTrainingForm::ChoiceTrainingForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChoiceTrainingForm)
{
    ui->setupUi(this);
}

ChoiceTrainingForm::~ChoiceTrainingForm()
{
    delete ui;
}

void ChoiceTrainingForm::clickIntervalDetector() {
    emit onIntervalDetector();
}

void ChoiceTrainingForm::clickTriadeDetector() {
    emit onTriadeDetector();
}

void ChoiceTrainingForm::clickNoteDetector() {
    emit onNoteDetector();
}

