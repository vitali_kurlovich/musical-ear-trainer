#ifndef UTILS_H
#define UTILS_H

#include <QObject>

class Utils : public QObject
{
    Q_OBJECT
public:
    explicit Utils(QObject *parent = 0);

public:
   static QString codeForNote(const int note);
   static QString nameForNote(const int note);

   static QString codeForTriad(const int triad) ;
   static QString nameForTriad(const int triad) ;

   static QString codeForInterval(const int interval);
   static QString nameForInterval(const int interval);
};

#endif // UTILS_H
