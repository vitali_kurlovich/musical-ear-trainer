#ifndef CHOICETRAININGFORM_H
#define CHOICETRAININGFORM_H

#include <QWidget>

namespace Ui {
class ChoiceTrainingForm;
}

class ChoiceTrainingForm : public QWidget
{
    Q_OBJECT

public:
    explicit ChoiceTrainingForm(QWidget *parent = 0);
    ~ChoiceTrainingForm();

signals:
    void onIntervalDetector();
    void onTriadeDetector();
    void onNoteDetector();

protected slots:
    void clickIntervalDetector();
    void clickTriadeDetector();
    void clickNoteDetector();

private:
    Ui::ChoiceTrainingForm *ui;
};

#endif // CHOICETRAININGFORM_H
