#include "databasehelper.h"

#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QSqlError>
#include <QSqlDatabase>
#include <QCoreApplication>

DataBaseHelper::DataBaseHelper(QObject *parent) :
    QObject(parent)
{
}

// базовый путь к звуковым файлам
QString DataBaseHelper::basePath() {
    QString basePath = QCoreApplication::applicationDirPath() + "/data/sounds";

    // для отладочного режима меняем путь на путь к файлам проекта
#ifdef QT_DEBUG
    basePath =   "../../../../MusicalEarTrainer/data/sounds";
#endif
    return basePath;
}

// получаем путь по ноте
QString DataBaseHelper::urlForNote(const QString& note) {
    QString sql = tr("SELECT url FROM notes WHERE note = '%1'").arg(note);

    qDebug() << sql;

    QSqlQuery query;
    if (!query.exec(sql)) {
        qDebug() << query.lastError().text() ;
    }

    if (query.first()) {
        QString path = basePath() + query.value(0).toString();
        return path;
    }

    return "";
}

// получаем путь по ноте и триаде
QString DataBaseHelper::urlForNote(const QString& note, const QString& triad) {

    QSqlQuery query;

    QString sql = tr("SELECT url FROM triade WHERE note = '%1' AND triad = '%2' ").arg(note).arg(triad);

    qDebug() << sql;
    if (!query.exec(sql)) {
        qDebug() << query.lastError().text() ;
    }

    if (query.first()) {
        QString path = basePath() + query.value(0).toString();
        return path;
    }

    return "";
}


// получаем путь по ноте и интервалу
QString DataBaseHelper::urlForNoteAndInterval(const QString& note, const QString& interval) {


    QString sql = tr("SELECT url FROM intervals WHERE note = '%1' AND interval = '%2'").arg(note).arg(interval);

    qDebug() << sql;

    QSqlQuery query;
    if (!query.exec(sql)) {
        qDebug() << query.lastError().text() ;
    }

    if (query.first()) {
        QString path = basePath() + query.value(0).toString();
        return path;
    }

    return "";
}
