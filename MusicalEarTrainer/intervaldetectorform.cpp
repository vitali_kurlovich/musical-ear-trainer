#include "intervaldetectorform.h"
#include "ui_intervaldetectorform.h"

#include "databasehelper.h"
#include "utils.h"

#include <QSoundEffect>
#include <QDebug>

IntervalDetectorForm::IntervalDetectorForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IntervalDetectorForm)
{
    ui->setupUi(this);
    doShot();
}

IntervalDetectorForm::~IntervalDetectorForm()
{
    delete ui;
}

// в главное меню
void IntervalDetectorForm::clickBack() {
    emit onBack();
}

// следующий вариант
void IntervalDetectorForm::clickNext() {
    doShot();
}

// генерируем случайный вариант
void IntervalDetectorForm::doShot() {
    currentNote = rand()%7 + 1; //  случайная нота
    currentInterval = rand()%12 + 1; //  случайный интервал

    // настройка  интерфейса
    ui->nextButton->setEnabled(false);
    ui->result_label->setText("");
    ui->answerLabel->setText("");

    qDebug() << "Current note:" << currentNote << " curent Interval: "  << currentInterval;

    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(currentInterval)); // проигрываем сгенерированный вариант
    enableButtons(true); // делаем активными кнопки выбора варианта
}

bool IntervalDetectorForm::isCorrectInterval(const int interval) const {
    return (interval == currentInterval);
}

// проверка выбранного интервала
void IntervalDetectorForm::checkInterval(const int interval) const {
    qDebug() << "Check Interval:" << interval;

    if (isCorrectInterval(interval)) {
        ui->result_label->setText("Правильно");
        ui->result_label->setStyleSheet("color:green");
        ui->answerLabel->setText(tr("Правильный ответ:  %1").arg(Utils::nameForInterval(currentInterval)));
    } else {
        ui->result_label->setText("Не правильно");
        ui->result_label->setStyleSheet("color:red");
        ui->answerLabel->setText(tr("Правильный ответ:  %1").arg(Utils::nameForInterval(currentInterval)));
    }

    ui->nextButton->setEnabled(true); // делаем активной кнопку 'Продолжить'
    enableButtons(false); // делаем не активными кнопки выбора варианта
}

// проиграли текущую ноту
void IntervalDetectorForm::clickReplay() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(currentInterval));
}

// проиграть ноту с указаным интервалом
void IntervalDetectorForm::playSoundForNote(const QString& note, const QString& interval) const {

    QString url = DataBaseHelper::urlForNoteAndInterval(note, interval); // получаем из БД путь к звуковому файлу
    qDebug() << url;

    if (!url.isEmpty()) { // если путь не пустой проигрываем звук
        QSoundEffect* sound = new QSoundEffect();
        sound->setSource(QUrl::fromLocalFile(url));
        sound->play();
    }
}

// установка свойство enable для кнопок выбора ответа
void IntervalDetectorForm::enableButtons(bool enable) const {
    ui->pushButton_1->setEnabled(enable);
    ui->pushButton_2->setEnabled(enable);
    ui->pushButton_3->setEnabled(enable);
    ui->pushButton_4->setEnabled(enable);
    ui->pushButton_5->setEnabled(enable);
    ui->pushButton_6->setEnabled(enable);
    ui->pushButton_7->setEnabled(enable);
    ui->pushButton_8->setEnabled(enable);
    ui->pushButton_9->setEnabled(enable);
    ui->pushButton__10->setEnabled(enable);
    ui->pushButton__11->setEnabled(enable);
    ui->pushButton__12->setEnabled(enable);
}

// нажали на кнопку 'Чистая прима'
void IntervalDetectorForm::click1() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(1)); // проиграли звук связанный с текущей нотой и интервалом 'Чистая прима'
    checkInterval(1); // проверили вариант ''Чистая прима'
}

// "Малая секунда”
void IntervalDetectorForm::click2() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(2));
    checkInterval(2);
}

// “Большая секунда”
void IntervalDetectorForm::click3() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(3));
    checkInterval(3);
}

// “Малая терция”
void IntervalDetectorForm::click4() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(4));
    checkInterval(4);
}

// “Большая терция”
void IntervalDetectorForm::click5() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(5));
    checkInterval(5);
}

// “Чистая кварта”
void IntervalDetectorForm::click6() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(6));
    checkInterval(6);
}

// “Чистая квинта”
void IntervalDetectorForm::click7() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(7));
    checkInterval(7);
}

// “Малая секста”
void IntervalDetectorForm::click8() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(8));
    checkInterval(8);
}

// “Большая секста”
void IntervalDetectorForm::click9() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(9));
    checkInterval(9);
}

// “Малая септима”
void IntervalDetectorForm::click10() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(10));
    checkInterval(10);
}

// “Большая септима”
void IntervalDetectorForm::click11() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(11));
    checkInterval(11);
}

// “Октава”
void IntervalDetectorForm::click12() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForInterval(12));
    checkInterval(12);
}
