#include "triaddetectorform.h"
#include "ui_triaddetectorform.h"
#include "databasehelper.h"
#include "utils.h"

#include <QSoundEffect>
#include <QDebug>

TriadDetectorForm::TriadDetectorForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TriadDetectorForm)
{
    ui->setupUi(this);
    doShot();
}

TriadDetectorForm::~TriadDetectorForm()
{
    delete ui;
}

void TriadDetectorForm::clickBack() {
    // назад
    emit onBack();
}

// кнопка 'Продолжить'
void TriadDetectorForm::clickNext() {
    doShot();
}

// генерируем случайный вариант
void TriadDetectorForm::doShot() {
    currentNote = rand()%7 + 1; //  случайная нота
    currentTriade= rand()%2 + 1; //  случайная триада

    // настройка  интерфейса
    ui->nextButton->setEnabled(false);
    ui->result_label->setText("");
    ui->answerLabel->setText("");

    qDebug() << "Current note:" << currentNote << " Triade: " << currentTriade;

    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForTriad(currentTriade));// проигрываем сгенерированный вариант
    enableTriadButtons(true);  // делаем активными кнопки выбора варианта
}

// выбрали 'Мажорное трезвучие'
void TriadDetectorForm::clickMajor() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForTriad(1)); // проиграть мажорное трезвучие для выбранной ноты
    checkTriad(1); // проверка результата
}

// выбрали 'Минорное трезвучие'
void TriadDetectorForm::clickMinor() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForTriad(2)); // проиграть минорное трезвучие для выбранной ноты
    checkTriad(2); // проверка результата
}

bool TriadDetectorForm::isCorrectTriad(const int triad) const {
    return (triad == currentTriade);
}

void TriadDetectorForm::checkTriad(const int triad) const {
    qDebug() << "Check triad:" << triad;

    // обработка результата
    if (isCorrectTriad(triad)) {
        ui->result_label->setText("Правильно");
        ui->result_label->setStyleSheet("color:green");
        ui->answerLabel->setText(tr("Правильный ответ:  %1").arg(Utils::nameForTriad(currentTriade)));
    } else {
        ui->result_label->setText("Не правильно");
        ui->result_label->setStyleSheet("color:red");
        ui->answerLabel->setText(tr("Правильный ответ:  %1").arg(Utils::nameForTriad(currentTriade)));
    }

    ui->nextButton->setEnabled(true); // делаем активной кнопку 'Продолжить'
    enableTriadButtons(false);  // делаем неактивными кнопки выбора варианта
}

// установка свойство enable для кнопок выбора ответа
void TriadDetectorForm::enableTriadButtons(bool enable) const {
    ui->pushButtonMinor->setEnabled(enable);
    ui->pushButtonMajor->setEnabled(enable);
}

// кнопка 'Повторить'
void TriadDetectorForm::clickReplay() {
    playSoundForNote(Utils::codeForNote(currentNote), Utils::codeForTriad(currentTriade));
}

// проиграть ноту из триады
void TriadDetectorForm::playSoundForNote(const QString& note, const QString triad) const {

    QString url = DataBaseHelper::urlForNote(note, triad); // получаем из БД путь к звуковому файлу
    qDebug() << url;
    if (!url.isEmpty()) { // если путь не пустой проигрываем звук
        QSoundEffect* sound = new QSoundEffect();
        sound->setSource(QUrl::fromLocalFile(url));
        sound->play();
    }
}
